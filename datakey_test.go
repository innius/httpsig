package httpsig

import (
	"github.com/aws/aws-sdk-go/service/kms"
	"github.com/aws/aws-sdk-go/service/kms/kmsiface"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

type kmsMock struct {
	kmsiface.KMSAPI
}

func (m *kmsMock) GenerateDataKey(*kms.GenerateDataKeyInput) (*kms.GenerateDataKeyOutput, error) {
	return &kms.GenerateDataKeyOutput{
		Plaintext:      []byte("your secret"),
		CiphertextBlob: []byte("encrypted data key"),
	}, nil
}

func TestDataKeyFactory(t *testing.T) {
	Convey("Create a new datakey for my service", t, func() {
		ks := dataKeyService{&kmsMock{}}
		serviceName := "foo-service"
		key, err := ks.generateDataKey(serviceName, "key-arn")
		So(err, ShouldBeNil)
		So(key, ShouldNotBeNil)
		So(key.Id, ShouldNotBeEmpty)
		So(key.ServiceName, ShouldEqual, serviceName)
		So(key.CiphertextBlob, ShouldNotBeNil)
		So(key.Plaintext, ShouldNotBeNil)
	})
}
