package httpsig

import (
	"encoding/base64"
	"github.com/aws/aws-sdk-go/service/kms"
	"github.com/aws/aws-sdk-go/service/kms/kmsiface"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spacemonkeygo/httpsig"
	"net/http"
	"net/http/httputil"
)

type Verifier struct {
	verifier *httpsig.Verifier
	kmsapi   kmsiface.KMSAPI
	store    *memoryKeyStore
	log      *logrus.Logger
}

func NewVerifier(kmsapi kmsiface.KMSAPI) *Verifier {
	store := newMemoryKeyStore()
	return &Verifier{
		store:    store,
		verifier: httpsig.NewVerifier(store),
		log:      logrus.New(),
		kmsapi:   kmsapi}
}

func (v *Verifier) WithLogger(l *logrus.Logger) *Verifier {
	v.log = l
	return v
}

func (v *Verifier) Verify(r *http.Request) error {
	if err := v.verifyRequestSigningKey(r); err != nil {
		return err
	}
	if err := v.verifier.Verify(r); err != nil {
		return VerificationError{error: err}
	}
	return nil
}

// VerificationError indicates the request is not properly signed.
type VerificationError struct {
	error
}

// verifyRequestSigningKey gets the private key for the request by decrypting the encrypted data key with kms
func (v *Verifier) verifyRequestSigningKey(r *http.Request) error {
	if v.log.IsLevelEnabled(logrus.DebugLevel) {
		if b, err := httputil.DumpRequest(r, true); err == nil {
			v.log.Debugf("request\n: %s", string(b))
		}
	}
	keyID, err := v.extractKeyIDFromRequest(r)
	if err != nil {
		return VerificationError{error: err}
	}
	v.log.Debugf("get key %s from store", keyID)
	if _, exists := v.store.keys[keyID]; !exists {
		v.log.Debugf("extracting key %s from the request", keyID)
		key, err := v.extractDataKeyFromRequest(r)
		if err != nil {
			return VerificationError{error: err}
		}
		serviceName, err := v.getHeaderValue(r, HttpHeaderServiceName)
		if err != nil {
			return VerificationError{error: err}
		}

		res, err := v.kmsapi.DecryptWithContext(r.Context(), &kms.DecryptInput{
			CiphertextBlob:    key,
			EncryptionContext: encryptionContext(serviceName, keyID),
		})
		if err != nil {
			return errors.Wrap(err, "could not decrypt data key")
		}
		v.log.Debugf("decrypted the datakey %s", res.String())
		// keys are added to the store only once to limit the number of aws calls
		// note: keys do not expire.
		v.store.SetKey(keyID, res.Plaintext)
	}
	return nil
}

func (v *Verifier) extractKeyIDFromRequest(r *http.Request) (string, error) {
	return v.getHeaderValue(r, HttpHeaderKeyID)
}

func (v *Verifier) getHeaderValue(r *http.Request, key string) (string, error) {
	if s := r.Header.Get(key); s != "" {
		return s, nil
	}
	return "", errors.Errorf("invalid request: mandatory header %s not present", key)
}

// extractDataKeyFromRequest reads the base64 encoded value of the key header returns the decoded value
func (v *Verifier) extractDataKeyFromRequest(r *http.Request) ([]byte, error) {
	s, err := v.getHeaderValue(r, HttpHeaderEncryptedDataKey)
	if err != nil {
		return nil, err
	}
	dataKey, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	return dataKey, nil
}
