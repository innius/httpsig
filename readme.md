# HTTPSIG for Go

This library implements HTTP request signature generation and verification based on
the RFC draft specification https://tools.ietf.org/html/draft-cavage-http-signatures-06.

Http requests are signed according to this spec; in addition to the signuature some innius specific header are added to the request. These headers are required for request verification within the innius stack.  

![](https://codebuild.us-east-1.amazonaws.com/badges?uuid=eyJlbmNyeXB0ZWREYXRhIjoib0htNDQvM2ZPRjVoeG1PWk1PWEVrRVVoYjhaOTZEdWNSOXpoekVLNHU1ZHFmWVpTRjd4UU1wYi9oWHI2bmRJaGZ0T3BOWGZ3QkUzZjFyM295b2FQNXBJPSIsIml2UGFyYW1ldGVyU3BlYyI6ImlSQ1RWb2hDcTdybUNQWVoiLCJtYXRlcmlhbFNldFNlcmlhbCI6MX0%3D&branch=master)

## signing

``` 
dataKey, err := NewServiceDataKey("foo-service", "kms arn from service config")
....
signer := NewRequestSigner(dataKey)

signer.Sign(r)
```
## verify
``` 
verifier := NewVerifier(kmsapi)

err := verifier.Verify(req)
```


 