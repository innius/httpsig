package httpsig_test

import (
	"bitbucket.org/innius/go-api-context"
	"bitbucket.org/innius/httpsig"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/kms"
	"github.com/aws/aws-sdk-go/service/kms/kmsiface"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func NewRequest() *http.Request {
	req := httptest.NewRequest("POST", "http://example.com/foo",
		strings.NewReader(`{"hello": "world"}`))

	req.Header.Set("Date", "Thu, 05 Jan 2014 21:31:40 GMT")
	req.Header.Set("Content-Type", "application/json")

	return req
}

type kmsMock struct {
	kmsiface.KMSAPI
	plaintext []byte
	counter   int
}

func (m *kmsMock) DecryptWithContext(aws.Context, *kms.DecryptInput, ...request.Option) (*kms.DecryptOutput, error) {
	m.counter++
	return &kms.DecryptOutput{
		Plaintext: m.plaintext,
	}, nil
}

func TestSignAndVerify(t *testing.T) {
	dataKey := &httpsig.DataKey{
		ServiceName:    "the-foo-service",
		Id:             "kms-key-id",
		Plaintext:      []byte("plaintext encryption key"),
		CiphertextBlob: []byte("encrypted data key from kms"),
	}
	Convey("Signing", t, func() {
		signer := httpsig.NewRequestSigner(dataKey)
		req := NewRequest()
		assert.NoError(t, signer.Sign(req))

		Convey("the signed request", func() {
			Convey("must have a signature", func() {
				So(req.Header, ShouldContainKey, http.CanonicalHeaderKey("authorization"))
			})
			Convey("must have an encrypted datakey header", func() {
				So(req.Header, ShouldContainKey, httpsig.HttpHeaderEncryptedDataKey)
			})
			Convey("must have an innius service name header", func() {
				So(req.Header, ShouldContainKey, httpsig.HttpHeaderServiceName)
			})
		})
		Convey("Verification", func() {
			mock := &kmsMock{plaintext: dataKey.Plaintext}
			verifier := httpsig.NewVerifier(mock)
			Convey("the signature should must be valid", func() {
				So(verifier.Verify(req), ShouldBeNil)
				Convey("validate request with same key again", func() {
					So(verifier.Verify(req), ShouldBeNil)
					Convey("should not result in a kms Decrypt call", func() {
						So(mock.counter, ShouldEqual, 1)
					})
				})
			})
			Convey("tamper the service-name", func() {
				req.Header.Set(httpsig.HttpHeaderServiceName, "bar-service")
				Convey("should return in error", func() {
					So(verifier.Verify(req), ShouldBeError)
				})
			})
		})
	})
	Convey("With api context", t, func() {
		// set appropriate context in the request
		const companyID = "foo"
		const userID = "bar"
		const domain = "bar.com"

		req := NewRequest()
		c := apicontext.From(req.Context())
		ctx := apicontext.WithDomain(apicontext.WithUserID(apicontext.WithCompanyID(c, companyID), userID), domain)
		signer := httpsig.NewRequestSigner(dataKey)

		Convey("Sign the request", func() {
			So(signer.Sign(req.WithContext(ctx)), ShouldBeNil)
			Convey("the request headers must have context headers", func() {
				So(req.Header.Get(httpsig.HttpHeaderCompanyID), ShouldEqual, companyID)
				So(req.Header.Get(httpsig.HttpHeaderUserID), ShouldEqual, userID)
				So(req.Header.Get(httpsig.HttpHeaderDomain), ShouldEqual, domain)
			})
			Convey("the signature", func() {
				Convey("must include context headers", func() {
					signature := req.Header.Get(http.CanonicalHeaderKey("authorization"))
					So(strings.Contains(signature, strings.ToLower(httpsig.HttpHeaderCompanyID)), ShouldBeTrue)
					So(strings.Contains(signature, strings.ToLower(httpsig.HttpHeaderUserID)), ShouldBeTrue)
					So(strings.Contains(signature, strings.ToLower(httpsig.HttpHeaderDomain)), ShouldBeTrue)
				})
			})
			Convey("Validate the signature", func() {
				mock := &kmsMock{plaintext: dataKey.Plaintext}
				verifier := httpsig.NewVerifier(mock)
				Convey("the signature should must be valid", func() {
					So(verifier.Verify(req), ShouldBeNil)
					Convey("validate request with same key again", func() {
						So(verifier.Verify(req), ShouldBeNil)
						Convey("should not result in a kms Decrypt call", func() {
							So(mock.counter, ShouldEqual, 1)
						})
					})
				})
				Convey("tamper with the context headers", func() {
					req.Header.Add(httpsig.HttpHeaderCompanyID, "baz")
					So(verifier.Verify(req), ShouldBeError)
				})
				Convey("add additional headers to the signed request", func() {
					req.Header.Add("Accept-Encoding", "gzip")
					req.Header.Add("Cloudfront-Forwarded-Proto", "https")
					req.Header.Add("Cloudfront-Is-Desktop-Viewer", "true")
					req.Header.Add("Cloudfront-Is-Mobile-Viewer", "false")
					req.Header.Add("Cloudfront-Is-Smarttv-Viewer", "false")
					req.Header.Add("Cloudfront-Is-Tablet-Viewer", "false")
					req.Header.Add("Cloudfront-Viewer-Country:", "US")
					req.Header.Add("Content-Type:", "application/json")
					req.Header.Add("User-Agent:", "Go-http-client/2.0")
					req.Header.Add("Via:", "2.0 dd6a13d6510988eea7236b9a3cd830fe.cloudfront.net (CloudFront)")
					req.Header.Add("X-Amz-Cf-Id", "l0-PCH_vE6tgFo3IDfssU_9bizfvAdxQfVpbXjc3k8UZOQ4U2Jr0XQ==")
					req.Header.Add("X-Amzn-Trace-Id", "Root=1-5c9ce620-d97695f585bdf6a643f68046")
					req.Header.Add("X-Forwarded-For", "34.236.253.169, 70.132.60.84")
					req.Header.Add("X-Forwarded-Port", "443")
					req.Header.Add("X-Forwarded-Proto", "https")
					So(verifier.Verify(req), ShouldBeNil)
				})
			})
		})
	})
}
