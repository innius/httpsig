module bitbucket.org/innius/httpsig

require (
	bitbucket.org/innius/go-api-context v1.1.5
	github.com/aws/aws-sdk-go v1.15.81
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.4.0
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a
	github.com/spacemonkeygo/httpsig v0.0.0-20180701154948-c76789e3b41d
	github.com/stretchr/testify v1.2.2
	github.com/taskcluster/slugid-go v1.0.1-0.20180913150312-f39bc6d245da
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.0.0 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20190309154008-847fc94819f9 // indirect
	github.com/jmespath/go-jmespath v0.0.0-20160202185014-0b12d6b521d8 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/smartystreets/assertions v0.0.0-20180927180507-b2de0cb4f26d // indirect
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2 // indirect
	golang.org/x/net v0.0.0-20190327214358-63eda1eb0650 // indirect
	golang.org/x/sys v0.0.0-20190215142949-d0b11bdaac8a // indirect
)
