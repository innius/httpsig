package httpsig

import "net/http"

var HttpHeaderEncryptedDataKey = http.CanonicalHeaderKey("x-innius-encrypted-datakey")

var HttpHeaderServiceName = http.CanonicalHeaderKey("x-innius-service-name")

var HttpHeaderCompanyID = http.CanonicalHeaderKey("x-innius-company-id")
var HttpHeaderUserID = http.CanonicalHeaderKey("x-innius-user-id")
var HttpHeaderDomain = http.CanonicalHeaderKey("x-innius-domain")

// this header is part of the encryption context for kms and it is required for request verification.
var HttpHeaderKeyID = http.CanonicalHeaderKey("x-innius-key-id")

