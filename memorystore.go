package httpsig

import "sync"

type memoryKeyStore struct {
	keys map[string]interface{}
}

func newMemoryKeyStore() *memoryKeyStore {
	return &memoryKeyStore{
		keys: make(map[string]interface{}),
	}
}

var lock sync.Mutex

func (m *memoryKeyStore) GetKey(id string) interface{} {
	return m.keys[id]
}

func (m *memoryKeyStore) SetKey(id string, key interface{}) {
	defer lock.Unlock()
	lock.Lock()

	m.keys[id] = key
}
