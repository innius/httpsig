package httpsig

import (
	"bitbucket.org/innius/go-api-context"
	"encoding/base64"
	"github.com/sirupsen/logrus"
	"github.com/spacemonkeygo/httpsig"
	"net/http"
	"net/http/httputil"
)

// RequestSigner is an http request signer for innius services
type RequestSigner struct {
	signer     *httpsig.Signer
	signingKey *DataKey
	log        *logrus.Logger
	dataKey    string // base64 encoded CiphertextBlob
}

var headers = []string{"(request-target)", "host", "date", HttpHeaderServiceName, HttpHeaderCompanyID, HttpHeaderUserID, HttpHeaderDomain}

// NewRequestSigner returns a new request signer which can be passed to go-sdk
func NewRequestSigner(key *DataKey) *RequestSigner {
	return &RequestSigner{
		signer:     httpsig.NewHMACSHA256Signer(key.Id, key.Plaintext, headers),
		dataKey:    base64.StdEncoding.EncodeToString(key.CiphertextBlob),
		signingKey: key,
		log:        logrus.New(),
	}
}

func (s *RequestSigner) WithLogger(l *logrus.Logger) *RequestSigner {
	s.log = l
	return s
}

// Sign signs an http request and adds the signature to the authorization header
func (s *RequestSigner) Sign(r *http.Request) error {
	debug := s.log.IsLevelEnabled(logrus.DebugLevel)

	if debug {
		if b, err := httputil.DumpRequest(r, true); err == nil {
			s.log.Debugf("request\n: %s", string(b))
		}
	}

	r.Header.Add(HttpHeaderServiceName, s.signingKey.ServiceName)
	r.Header.Add(HttpHeaderEncryptedDataKey, s.dataKey)
	r.Header.Add(HttpHeaderKeyID, s.signingKey.Id)

	// add the context specific headers to the request
	c := apicontext.From(r.Context())
	r.Header.Add(HttpHeaderCompanyID, c.Company())
	r.Header.Add(HttpHeaderUserID, c.UserID())
	r.Header.Add(HttpHeaderDomain, c.Domain())

	err := s.signer.Sign(r)
	if err != nil {
		return err
	}
	if debug {
		if b, err := httputil.DumpRequest(r, true); err == nil {
			s.log.Debugf("signed request\n: %s", string(b))
		}
	}
	return nil

}
