package httpsig

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kms"
	"github.com/aws/aws-sdk-go/service/kms/kmsiface"
	"github.com/pkg/errors"
	"github.com/taskcluster/slugid-go/slugid"
)

// DataKey represents a singing key with a Plaintext to encrypt data locally and a CiphertextBlob which
// can be sent along with the request; Allowing the other side to get the private key via kms.
type DataKey struct {
	Id             string
	ServiceName    string
	Plaintext      []byte
	CiphertextBlob []byte
}

// convenience method for creating a service data key with kms.
func NewServiceDataKey(serviceName string, kmsKeyArn string) (*DataKey, error) {
	sess := session.Must(session.NewSession())
	k := &dataKeyService{kms.New(sess)}
	return k.generateDataKey(serviceName, kmsKeyArn)
}

type dataKeyService struct {
	kmsiface.KMSAPI
}

func (s *dataKeyService) generateDataKey(serviceName string, kmsKeyArn string) (*DataKey, error) {
	keyID := slugid.Nice()

	key, err := s.GenerateDataKey(&kms.GenerateDataKeyInput{
		EncryptionContext: encryptionContext(serviceName, keyID),
		KeyId:             aws.String(kmsKeyArn),
		KeySpec:           aws.String("AES_256"),
	})

	if err != nil {
		return nil, errors.Wrapf(err, "could not create a data key for service %s", serviceName)
	}
	return &DataKey{Id: keyID, Plaintext: key.Plaintext, CiphertextBlob: key.CiphertextBlob, ServiceName: serviceName}, nil
}

func encryptionContext(serviceName, keyID string) map[string]*string {
	return map[string]*string{
		HttpHeaderServiceName: aws.String(serviceName),
		HttpHeaderKeyID:       aws.String(keyID),
	}

}
